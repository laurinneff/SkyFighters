# SkyFighters
A Minecraft plugin that adds a new Minigame

[Builds](https://gitlab.com/laurinneff/SkyFighters/pipelines)  
[Latest Successful Version](https://gitlab.com/laurinneff/SkyFighters/-/jobs/artifacts/master/raw/build/libs/SkyFighters.jar?job=build)

# Test Server
IP: server.mcgoodcraft.tk
Use "/server minigames" to join the test server
Thanks to u/prosteDeni (reddit) or @prosteDeni#3659 (discord) for hosting the server
