package ch.laurinneff.skyfighters;

import ch.laurinneff.skyfighters.commands.SkyFightersCommand;
import ch.laurinneff.skyfighters.listeners.Listeners;
import io.papermc.lib.PaperLib;
import org.bukkit.Bukkit;
import org.bukkit.GameRule;
import org.bukkit.Material;
import org.bukkit.command.PluginCommand;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.slf4j.Logger;

import java.io.*;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

public class SkyFighters extends JavaPlugin {
    public static SkyFighters instance;
    public List<Ring> rings;
    public List<Weapon> weapons;
    public static boolean enabled = true;

    Logger logger;

    static {
        ConfigurationSerialization.registerClass(Ring.class, "Ring");
        ConfigurationSerialization.registerClass(Weapon.class, "Weapon");
    }

    public void onEnable() {
        instance = this;
        PaperLib.suggestPaper(this);
        PluginCommand skyFightersCommand = getCommand("skyfighters");
        logger = getSLF4JLogger();

        assert skyFightersCommand != null;
        skyFightersCommand.setExecutor(new SkyFightersCommand());
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new Listeners(), this);
        Bukkit.getWorlds().forEach(world -> world.setGameRule(GameRule.DO_DAYLIGHT_CYCLE, false));
        Bukkit.getWorlds().forEach(world -> world.setGameRule(GameRule.DO_WEATHER_CYCLE, false));
        Bukkit.getWorlds().forEach(world -> world.setGameRule(GameRule.NATURAL_REGENERATION, false));
        FileConfiguration config = this.getConfig();

        // Config Defaults
        config.addDefault("rings", new Ring[]{new Ring(Ring.RingType.BOOST, 0, 0, 0, 0, 0, 0)});
        config.addDefault("weapons", new Weapon[]{new Weapon("Fireball", new ItemStack(Material.FIRE_CHARGE), "fireball", 3, 3, 1)});
        config.addDefault("spawnArea.x1", 0);
        config.addDefault("spawnArea.y1", 0);
        config.addDefault("spawnArea.z1", 0);
        config.addDefault("spawnArea.x2", 0);
        config.addDefault("spawnArea.y2", 0);
        config.addDefault("spawnArea.z2", 0);
        config.addDefault("spawnPoint.x", 0);
        config.addDefault("spawnPoint.y", 0);
        config.addDefault("spawnPoint.z", 0);
        config.addDefault("spawnPoint.yaw", 0);
        config.addDefault("spawnPoint.pitch", 0);
        config.addDefault("autoupdate", true);

        config.options().copyDefaults(true);
        saveConfig();

        rings = config.getObject("rings", List.class);
        weapons = config.getObject("weapons", List.class);

        Thread thread = new Thread(() -> {
            logger.info("Checking for Updates");

            String tempDir = Paths.get(getDataFolder().toString(), "temp").toString();

            new File(tempDir).mkdirs();
            URL website = null;
            try {
                website = new URL("https://gitlab.com/laurinneff/SkyFighters/-/jobs/artifacts/master/raw/build/libs/SkyFighters.jar?job=build");
                ReadableByteChannel rbc = Channels.newChannel(website.openStream());
                FileOutputStream fos = new FileOutputStream(Paths.get(tempDir, "SkyFighters.jar").toString());
                fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
                fos.close();
                rbc.close();
                String downloadHash = md5(Paths.get(tempDir, "SkyFighters.jar").toString());
                String selfPath = Paths.get("plugins", new File(SkyFighters.class.getProtectionDomain() // this gets my jars name
                        .getCodeSource()
                        .getLocation()
                        .getPath())
                        .getName()).toString();
                String selfHash = md5(selfPath);

                if (!downloadHash.equals(selfHash)) {
                    logger.info("A New Version is available. The Plugin will be updated on the next plugin reload");
                    InputStream is = null;
                    OutputStream os = null;
                    try {
                        is = new FileInputStream(Paths.get(tempDir, "SkyFighters.jar").toString());
                        os = new FileOutputStream(selfPath);
                        byte[] buffer = new byte[1024];
                        int length;
                        while ((length = is.read(buffer)) > 0) {
                            os.write(buffer, 0, length);
                        }
                    } finally {
                        assert is != null;
                        assert os != null;
                        is.close();
                        os.close();
                    }
                }
                else
                    logger.info("The Plugin is up to date");

                deleteDirectory(new File(tempDir));
            } catch (IOException | NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        });

        if (getConfig().getBoolean("autoupdate"))
            thread.start();
    }

    public void onDisable() {
    }

    boolean deleteDirectory(File directoryToBeDeleted) {
        File[] allContents = directoryToBeDeleted.listFiles();
        if (allContents != null) {
            for (File file : allContents) {
                deleteDirectory(file);
            }
        }
        return directoryToBeDeleted.delete();
    }

    String md5(String inputFile) throws NoSuchAlgorithmException, IOException {
        MessageDigest digest = MessageDigest.getInstance("MD5");
        FileInputStream fis = new FileInputStream(new File(inputFile));

        byte[] byteArray = new byte[1024];
        int bytesCount = 0;

        while ((bytesCount = fis.read(byteArray)) != -1) {
            digest.update(byteArray, 0, bytesCount);
        }

        fis.close();

        byte[] bytes = digest.digest();

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }

        return sb.toString();
    }
}
