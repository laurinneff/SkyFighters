package ch.laurinneff.skyfighters;

import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.inventory.ItemStack;

import java.util.*;

@SerializableAs("Weapon")
public class Weapon implements ConfigurationSerializable {
    public String name;
    public ItemStack item;
    public String action;
    public int damage;
    public int charges;
    public int speed;

    public Weapon(String name, ItemStack item, String action, int damage, int charges, int speed) {
        this.name = name;
        this.item = item;
        this.action = action;
        this.damage = damage;
        this.charges = charges;
        this.speed = speed;
    }

    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<>();
        map.put("name", name);
        map.put("item", item);
        map.put("action", action);
        map.put("damage", damage);
        map.put("charges", charges);
        map.put("speed", speed);
        return map;
    }

    public static Weapon deserialize(Map<String, Object> map) {
        return new Weapon((String) map.get("name"), (ItemStack) map.get("item"), (String) map.get("action"), (int) map.get("damage"), (int) map.get("charges"), (int) map.get("speed"));
    }
}
