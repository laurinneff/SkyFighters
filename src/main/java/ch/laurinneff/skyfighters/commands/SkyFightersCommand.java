package ch.laurinneff.skyfighters.commands;

import ch.laurinneff.skyfighters.SkyFighters;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class SkyFightersCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(args.length == 0) {
            sender.sendMessage(ChatColor.GRAY + "[" + ChatColor.GOLD + "SkyFighters" + ChatColor.GRAY + "] " + ChatColor.GREEN + "You forgot to tell me what to do. Here are the options:");
            sender.sendMessage(ChatColor.GRAY + "[" + ChatColor.GOLD + "SkyFighters" + ChatColor.GRAY + "] " + ChatColor.GREEN + "/" + label + " start" + ChatColor.GRAY + " : " + ChatColor.WHITE + "Enables all the events");
            sender.sendMessage(ChatColor.GRAY + "[" + ChatColor.GOLD + "SkyFighters" + ChatColor.GRAY + "] " + ChatColor.GREEN + "/" + label + " stop" + ChatColor.GRAY + " : " + ChatColor.WHITE + "Disables all the events");
            sender.sendMessage(ChatColor.GRAY + "[" + ChatColor.GOLD + "SkyFighters" + ChatColor.GRAY + "] " + ChatColor.GREEN + "/" + label + " config" + ChatColor.GRAY + " : " + ChatColor.WHITE + "Configure the plugin without editing the config (WIP)");
            return false;
        }

        switch (args[0]) {
            case "config":
                return false;
            case "start":
                SkyFighters.enabled = true;
                SkyFighters.instance.getServer().broadcastMessage(ChatColor.GRAY + "[" + ChatColor.GOLD + "SkyFighters" + ChatColor.GRAY + "] " + ChatColor.GREEN + "The Game is starting");
                return true;
            case "stop":
                SkyFighters.enabled = false;
                SkyFighters.instance.getServer().broadcastMessage(ChatColor.GRAY + "[" + ChatColor.GOLD + "SkyFighters" + ChatColor.GRAY + "] " + ChatColor.GREEN + "The Game is stopping");
                return true;
        }
        return false;
    }
}
